#
# Cookbook:: filebeat
# Recipe:: service
#
# Copyright:: 2019, The Authors, All Rights Reserved.

service 'filebeat' do
  action [node['elastic']['filebeat']['state'].to_sym, node['elastic']['filebeat']['enabled'].to_sym]
end
