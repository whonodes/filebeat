#
# Cookbook:: filebeat
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.
include_recipe 'elastic-repo::default'

include_recipe 'filebeat::package'
include_recipe 'filebeat::service'
include_recipe 'filebeat::configure_unix' unless node['platform'] == 'windows'
include_recipe 'filebeat::configure_windows' if node['platform'] == 'windows'
