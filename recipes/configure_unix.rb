#
# Cookbook:: filebeat
# Recipe:: configure_unix
#
# Copyright:: 2019, The Authors, All Rights Reserved.

directory '/etc/filebeat' do
  action :create
  owner 'root'
  group 'root'
  mode '0755'
end

directory '/usr/share/filebeat' do
  action :create
  owner 'root'
  group 'root'
  mode '0754'
end

directory '/var/lib/filebeat' do
  action :create
  owner 'root'
  group 'root'
  mode '0755'
end

directory '/var/log/filebeat' do
  action :create
  owner 'root'
  group 'root'
  mode '0755'
end

template '/etc/filebeat/filebeat.yml' do
  action :create
  owner 'root'
  group 'root'
  mode '0754'
  source 'filebeat.yml.erb'
  notifies :restart, 'service[filebeat]', :delayed
end
