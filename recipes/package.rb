#
# Cookbook:: filebeat
# Recipe:: package
#
# Copyright:: 2019, The Authors, All Rights Reserved.

package 'filebeat' do
  action node['elastic']['filebeat']['package'].to_sym
  notifies :stop, 'service[filebeat]', :before
  notifies :restart, 'service[filebeat]', :delayed
end
