#
# Cookbook:: filebeat
# Recipe:: configure_windows
#
# Copyright:: 2019, The Authors, All Rights Reserved.

directory 'C:\Program Files\Filebeat' do
  action :create
end
