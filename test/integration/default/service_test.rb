# InSpec test for recipe filebeat::service

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe service 'filebeat' do
  it { should be_running }
  it { should be_enabled }
end
