name 'filebeat'
maintainer 'Matthew Iverson'
maintainer_email 'matthewdiverson@gmail.com'
license 'MIT'
description 'Installs/Configures filebeat'
long_description 'Installs/Configures filebeat'
version '0.1.0'
chef_version '>= 14.0'

depends 'elastic-repo'
depends 'elastic-beats'

%w(centos fedora debian redhat ubuntu).each do |os|
  supports os
end

issues_url 'https://bitbucket.org/whonodes/filebeat/issues'
source_url 'https://bitbucket.org/whonodes/filebeat'
