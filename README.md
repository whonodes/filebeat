# filebeat

Used to install Elastic's filebeat service


### Dependencies
[elastic-repo](https://bitbucket.org/whonodes/elastic-repo)  
[elastic-beats](https://bitbucket.org/whonodes/elastic-beats)

### Issues
You can open issues for this cookbook [here](https://bitbucket.org/whonodes/filebeat/issues)


